﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.WebAPI.Controllers
{
	[ApiController]
	[Route("api/[controller]/[action]")]

	public class CustomerController : Controller
    {
		private CustomerRepository _repository;

		public CustomerController(CustomerRepository repository)
		{
			this._repository = repository;
		}

		[HttpGet("{id:int}")]
		public async Task <ActionResult<Customer>> GetById(int id)
		{
			var customer = await _repository.GetByIdAsync(id);
			if (customer is null)
				return NotFound();

			return Ok(customer);
		}


		[HttpPost]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status409Conflict)]
		
		public async Task<IActionResult> Add([FromBody]Customer customer)
		{
			var checkCustomer = await _repository.GetByIdAsync(customer.Id);

			if (checkCustomer is not null)
				return Conflict();

			_repository.AddCustomer(customer);
			_repository.SaveChanges();
			
			return Ok();
		}

	}
}


