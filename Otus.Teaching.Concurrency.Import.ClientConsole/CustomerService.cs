﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.ClientConsole
{
    class CustomerService
    {
        private static HttpClient _client;
        private static Uri _baseUri;

        public CustomerService(string baseUriString)
        {
            if (_client == null)
               _client= new HttpClient();
            _baseUri = new Uri(baseUriString);
        }

        public async Task<string> GetByIdAsync(int id)
        {
            var response = await _client.GetAsync(new Uri(_baseUri, $"/api/Customer/GetById/{id}"));

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> CreateAsync(Customer customer)
        {
            var response = await _client.PostAsJsonAsync(new Uri(_baseUri, $"/api/Customer/Add"), customer);

            if (response.IsSuccessStatusCode)
                return "Customer created";

            return $"Customer creation failed. Status code: {response.StatusCode}";
        }
    }

}

