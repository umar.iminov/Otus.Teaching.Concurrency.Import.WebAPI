﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System.Linq;
namespace Otus.Teaching.Concurrency.Import.ClientConsole
{
    class Program
    {
        private static string _baseUri = "https://localhost:44345";

        static async Task Main(string[] args)
        {
            var customerService = new CustomerService(_baseUri);
           
                while (true)
                {
                try
                {
                    Console.WriteLine(
                        $"Enter customer id to get Customer, Type Add to create new Customer " +
                        $"or type q to exit: ");

                    var input = Console.ReadLine();

                    if (input.Equals("q"))
                        break;

                    if (input.Equals("Add"))
                    {
                        Console.WriteLine("Enter ID");
                        int.TryParse(Console.ReadLine(), out int id);
                        var customer = RandomCustomerGenerator.Generate(id).LastOrDefault();//это трэш но мне лень переделывать :)
                        Console.WriteLine(await customerService.CreateAsync(customer));
                    }
                    else if (!int.TryParse(input, out var customerId))
                        Console.WriteLine("Incorrect input format");
                    else
                        Console.WriteLine(await customerService.GetByIdAsync(customerId));
                }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }


        Console.WriteLine("Good bye!");
            }
    }


}
