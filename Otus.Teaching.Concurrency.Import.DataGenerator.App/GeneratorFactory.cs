using Otus.Teaching.Concurrency.Import.Handler.Data;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, int dataCount, string fileType)
        { switch (fileType)
            {
                case "XML":
                    return new XmlDataGenerator(fileName, dataCount);
                case "CSV":
                    return new CsvGenerator(fileName, dataCount);
                default:
                    throw new InvalidOperationException($"��� ����� {fileType} �� ��������������");
            }
        }
    }
}