﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Loader;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders

{
    class DataLoaderThreadPool : IDataLoader
    {
        private readonly int _threadcount;
        private readonly List<Customer> _customers;
        public DataLoaderThreadPool(List<Customer> customers, int threadcount)
        {
            _customers = customers;
            _threadcount = threadcount;
        }

        public void LoadData()
        {
            var sw = new Stopwatch();

            Console.WriteLine($"Loading data... ThreadPool. ThreadCount = {_threadcount}");

            sw.Start();

            var partSize = _customers.Count / _threadcount;

            var handlers = new List<WaitHandle>();

            for (var i = 0; i < _threadcount; i++)
            {
                var handler = new AutoResetEvent(false);
                handlers.Add(handler);

                var query = _customers.OrderBy(x => x.Id).Skip(partSize * i);
                if (i != _threadcount - 1)
                {
                    query = query.Take(partSize);
                }

                ThreadPool.QueueUserWorkItem(LoadData, new StateInfo
                {
                    Customers = query.ToList(),
                    WaitHandle = handler
                });
            }

            WaitHandle.WaitAll(handlers.ToArray());

            sw.Stop();

            Console.WriteLine($"Loaded data. Mode ThreadPool. Took {sw.Elapsed.TotalSeconds:F}");
        }

        private static void LoadData(object state)
        {
            var ctx = new CustomerRepository();
            if (!(state is StateInfo stateInfo))
            {
                return;
            }

            ctx.AddCustomerRange(stateInfo.Customers);
            
            ctx.SaveChanges();

            stateInfo.WaitHandle.Set();
        }

        struct StateInfo
        {
            public AutoResetEvent WaitHandle { get; set; }
            public List<Customer> Customers { get; set; }
        }
    }
}