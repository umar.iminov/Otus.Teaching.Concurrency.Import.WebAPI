﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Loader;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders

{
    class DataLoaderThreads : IDataLoader
    {
        private readonly int _threadcount;
        private readonly List<Customer> _customers;
        public DataLoaderThreads(List<Customer> customers, int threadcount)
        {
            _customers = customers;
            _threadcount = threadcount;
        }

        public void LoadData()
        {
            var sw = new Stopwatch();

            Console.WriteLine($"Loading data... Threads. ThreadCount = {_threadcount}");

            sw.Start();

            var partSize = _customers.Count / _threadcount;

            List<Thread> listThread = new List<Thread>();
            for (var i = 0; i < _threadcount; i++)
            {
               var query = _customers.OrderBy(x => x.Id).Skip(partSize * i);
                if (i != _threadcount - 1)
                {
                    query = query.Take(partSize);
                }

                listThread.Add(new Thread(delegate () { LoadPart(query.ToList()); }));
                listThread[i].Start();
            }
            listThread.ForEach(x => x.Join());

            sw.Stop();

            Console.WriteLine($"Loaded data. Mode Threads. Took {sw.Elapsed.TotalSeconds:F}");
        }

    private static void LoadPart(List<Customer> customers)
        {
            var ctx = new CustomerRepository();

            ctx.AddCustomerRange(customers);

            ctx.SaveChanges();

    }
    
    }
}